#!/bin/bash

if [ $1 == "clean" ]
then
  rm distrib_additive_kernel.dat
  rm distrib_additive_kernel.ps
else
  cd ..
  make
  ./ishinisan test_additive_kernel/test_additive_kernel.nml
  mv distrib_additive_kernel.dat test_additive_kernel/
  cd test_additive_kernel
  gnuplot plot_additive_kernel.gp
  gv distrib_additive_kernel.ps
fi
