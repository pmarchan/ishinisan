set term post enh color
set output "distrib_additive_kernel.ps"

set xlabel "Grain mass m" font "Times-Roman,20"
set ylabel "Grain density {/Symbol r}" font "Times-Roman,20"

set xtics font "Times-Roman,20"
set ytics font "Times-Roman,20"

set format "10^{%L}"

set logscale

set xrange[1e-5:1e8]
set yrange [1e-10:1e0]
set key bot left font "Times-Roman,20"

plot "distrib_additive_kernel.dat" index 1 u 1:3 w lp lc 1 lw 2 tit "t=0",\
     "distrib_additive_kernel.dat" index 2 u 1:3 w p  lc 2 ps 1 not,\
     "distrib_additive_kernel.dat" index 2 u 1:7 w l  lc 2 lw 2 tit "t=2",\
     "distrib_additive_kernel.dat" index 3 u 1:3 w p  lc 3 ps 1 not,\
     "distrib_additive_kernel.dat" index 3 u 1:7 w l  lc 3 lw 2 tit "t=4",\
     "distrib_additive_kernel.dat" index 4 u 1:3 w p  lc 4 ps 1 not,\
     "distrib_additive_kernel.dat" index 4 u 1:7 w l  lc 4 lw 2 tit "t=6",\
     "distrib_additive_kernel.dat" index 5 u 1:3 w p  lc 5 ps 1 not,\
     "distrib_additive_kernel.dat" index 5 u 1:7 w l  lc 5 lw 2 tit "t=8"
