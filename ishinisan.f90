program ishinisan
  ! Main program Ishinisan
  USE variables
  USE part_trajectories
  implicit none
  character(127) :: paramfile           ! Namelist

  ! Integration
  integer :: itime                      ! For time iteration
  double precision :: dt,dtold          ! Time steps
  integer :: icoag1,icoag2              ! Bin in which coagulated
  double precision :: dndt              ! dn/dt change during coagulation
  double precision :: fracbin,fracbinup,fracbindown   ! fracbin=fracbinup/fracbindown= fraction of the coagulation product ending up in the lower bin
  double precision :: calcfrac          ! Function to calculate fracbin
  double precision :: calckern          ! Function to calculate the kernel
  double precision :: massini,masstot   ! Track of the total mass
  double precision :: mass_prec=0.05    ! Relative error allowed on the mass conservation

  ! Coagulation
  double precision :: kern              ! Coagulation kernel
  double precision :: maxratio          ! Maximum relative variation in one bin
  double precision :: mcoag1,mcoag2     ! Coagulated mass

  ! Trajectories
  integer :: idens,itemp,ipart          ! Index of the density, temperature and particle

  ! Other
  integer :: i,j,k,icount,i25,i75
  double precision :: dum
  double precision :: nhnext

  
  print*, ""
  print*, ""
  print*, "*************************************************************"
  print*, "* Thank you for using 石二三 (Ishinisan) by Pierre Marchand *"
  print*, "* Your calculation will begin shortly                       *"
  print*, "*************************************************************"
  print*, ""
  print*, ""

  ! Read parameters
  call getarg(1,paramfile)
  call read_params(paramfile)

  ! Initialize stuff
  call initialize()

  ! Calculate the slopes
  do ipart = 1,npart
    call calcslopes(ipart)
  end do

  Select case (distrib)
    case("con","add")
      massini=sum(rhobin(:,1))
    case default
      !massini=sum(rhobin(:,1)/nbin(:,1))
      massini=sum(rhobin(:,1)/nh)
  end select

  ! Calculate the charge
  do ipart=1,npart
    if(calc_charge) then
      if(calctype=="traj") then
        nh=partrho(ipart,1)
        T=parttemp(ipart,1)
      end if
      call charge(ipart)
      if(calc_resist) call resistivities(ipart)
    end if
  end do

  ! Print initial distribution
  do i=1,npart
    call output_distrib(.true.,i)
  end do



 ! Loop on particles 
 do ipart=1,npart   

    time=0
    chi=0
    itime=1
    dt=1d0
    icount=1

    if(calc_charge) psi=0d0


 !  Start integration

    print*, ""
    print*, "Starting time integration on particle #", ipart
    do while(time<timemax)

    if(calctype=="traj") then
      if(itime>ntimespart-1) exit
      time=parttime(itime)
      dt=parttime(itime+1)-parttime(itime)
      print*, "Output #",itime,"/",ntimespart-1
    end if

    drhobin(:,:)=0d0



      if(calctype=="traj") then
        nh=partrho(ipart,itime)
        T=parttemp(ipart,itime)
      end if

      maxratio=1d10

      if(kernel .ne. "N") then

        ! Double loop on bins
        do i=1,nbinmax
          do j=1,i

            ! In which bins are distributed the coagulated grains
            mcoag1=mbinmin(i)+mbinmin(j)
            mcoag2=mbinmax(i)+mbinmax(j)
            icoag1=floor(dlog(mcoag1/massmin)/log(eta)+1)
            icoag2=floor(dlog(mcoag2/massmin)/log(eta)+1)
            icoag1=min(icoag1,nbinmax)
            icoag2=min(icoag2,nbinmax)

            ! Calculating how the coagulation product is distributed over the 2 bins
            if(icoag1==icoag2) then
              fracbin=1d0
            else
              if(mbinmax(icoag1)-massbin(j,ipart)<mbinmin(i)) then
                fracbin=0d0
              else if (mbinmax(icoag1)-massbin(j,ipart)>mbinmax(i)) then
                fracbin=1d0
              else
                fracbinup=calcfrac(mbinmin(i),mbinmax(icoag1)-massbin(j,ipart),mbinmin(j),mbinmax(j),&
                                  &slopem(i,ipart),slopem(j,ipart),kernel)
                fracbindown=calcfrac(mbinmin(i),mbinmax(i),mbinmin(j),mbinmax(j),slopem(i,ipart),slopem(j,ipart),kernel)
                fracbin=fracbinup/fracbindown
              end if
            end if
            if(abs(fracbin-0.5d0)<=0.5d0) then
              dum=dum
            else
              print*, "PROBLEMO", ipart,i,j,fracbinup,fracbindown,fracbinup/fracbindown,fracbin
            end if

            ! Compute coag rate and rho variation
            ! Coagulation kernel
            kern=calckern(abin(i,ipart),abin(j,ipart),massbin(i,ipart),massbin(j,ipart))
            dndt=nbin(i,ipart)*nbin(j,ipart)*kern
            if(i==j) dndt=dndt/2d0
            drhobin(j,ipart)=drhobin(j,ipart)-massbin(j,ipart)*dndt
            drhobin(i,ipart)=drhobin(i,ipart)-massbin(i,ipart)*dndt
            drhobin(icoag1,ipart)=drhobin(icoag1,ipart)+fracbin*(massbin(i,ipart)+massbin(j,ipart))*dndt
            drhobin(icoag2,ipart)=drhobin(icoag2,ipart)+(1d0-fracbin)*(massbin(i,ipart)+massbin(j,ipart))*dndt


          end do
        end do  ! End double loop on bins


        ! Adjust timestep
        if(calctype .ne. "traj") then
          do i=1,nbinmax
            if(rhobin(i,ipart)/maxval(rhobin(:,ipart),1)>1d-20) maxratio=min(maxratio,rhobin(i,ipart)/abs(drhobin(i,ipart)*dt))
            !if(rhobin(i,ipart)>1d-300) maxratio=min(maxratio,rhobin(i,ipart)/abs(drhobin(i,ipart)*dt))
          end do
          dtold=dt
          dt=dt*(maxratio*varfact)
          if(time+dt>times(itime)) then
            dt=times(itime)-time
          end if
        end if

      end if ! End if kernel = N
      

      ! Update abundances
      do i=1,nbinmax
        rhobin(i,ipart)=max(rhobin(i,ipart)+drhobin(i,ipart)*dt,1d-300)
      end do

      if(calctype=="traj" .and. itime<ntimespart) then
        nhnext=partrho(ipart,itime+1)
        do i=1,nbinmax
          rhobin(i,ipart)=rhobin(i,ipart)*(1d0+(nhnext-nh)/nh)
        end do
      end if

      ! Compute the new slopes and moments
      call calcslopes(ipart)


      ! Check mass conservation
      Select case (distrib)
        case("con","add")
          masstot=sum(rhobin(:,ipart))
        case default
          !masstot=sum(rhobin(:,ipart)/nbin(:,ipart))
          masstot=sum(rhobin(:,ipart)/nh)
       end select
      if(abs(masstot-massini)/massini > mass_prec) then
        print*, "Mass not conserved !"
        print*, "Try increasing amax"
        print*, "Particle",ipart, "Mass = ", masstot, "Initial mass =", massini
        call output_distrib(.false.,ipart)
        stop
      end if

      ! Ionization calculation
      if(calc_charge) then
        call charge(ipart)
        if(calc_resist) then
          !if(calctype=="traj") Bmag=partmag(ipart,ntimespart)
          call resistivities(ipart)
        end if  
      end if



      ! Update time and output
      chi = chi+nh**0.75d0*T**(-0.25)*dt
      if(calctype=="traj") then
        if(itime<ntimespart) then
          itime=itime+1
          nh=partrho(ipart,itime)
          call output_distrib(.false.,ipart)
        end if
      else
        time=time+dt
        if(time>=times(itime)) then
          if(calctype=="coag") then
            write(*,'(A12,X,e8.3,X,A2)') "Output at t=",time/(86400d0*365d0),"yr"
          else
            write(*,'(A14,X,e8.3,X,A6)') "Output at Chi=",chi," (cgs)"
          end if
          itime=itime+1
          call output_distrib(.false.,1)
        end if
      end if

      if(itime>ntimesout) exit

      icount=icount+1
      if(mod(icount,1000)==0) print*, "t=",time/86400d0/365.25

    end do    ! End loop on time
  end do  ! End loop on particles

 print*,  ""
 print*,  "**********************"
 print*,  "Calculation successful"
 print*,  "**********************"
 print*,  ""


end program




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1



Double precision function calcfrac(mm1,mp1,mm2,mp2,s1,s2,kern)
  double precision, intent(in) :: mm1,mm2,mp1,mp2,s1,s2
  character(1),intent(in) :: kern
  double precision :: fact11,fact12,fact13,fact21,fact22,fact23
  double precision :: fact14,fact15,fact16,fact24,fact25,fact26
  double precision :: rate
  double precision :: fs1,fs2

  if(abs(s1)>5) then
    fs1=mm1
  else
    fs1=1d0
  end if
  if(abs(s2)>5) then
    fs2=mm2
  else
    fs2=1d0
  end if


  Select case (kern)

     case ("C")

       fact11=rate(mp1,mm1,s1,1d0)
       fact12=rate(mp1,mm1,s1,2d0)
       fact21=rate(mp2,mm2,s2,1d0)
       fact22=rate(mp2,mm2,s2,2d0)


       calcfrac=fs2*fact11*fact22+fs1*fact12*fact21


     case ("A")

       fact11=rate(mp1,mm1,s1,1d0)
       fact12=rate(mp1,mm1,s1,2d0)
       fact13=rate(mp1,mm1,s1,3d0)
       fact21=rate(mp2,mm2,s2,1d0)
       fact22=rate(mp2,mm2,s2,2d0)
       fact23=rate(mp2,mm2,s2,3d0)

       calcfrac=fs1**2d0*fact13*fact21+fs1*fs2*2d0*fact12*fact22+fs2**2d0*fact11*fact23


     case ("O")

       fact11=rate(mp1,mm1,s1,1d0+1d0/6d0)
       fact12=rate(mp1,mm1,s1,1d0+3d0/6d0)
       fact13=rate(mp1,mm1,s1,1d0+5d0/6d0)
       fact14=rate(mp1,mm1,s1,1d0+7d0/6d0)
       fact15=rate(mp1,mm1,s1,1d0+9d0/6d0)
       fact16=rate(mp1,mm1,s1,1d0+11d0/6d0)

       fact21=rate(mp2,mm2,s2,1d0+0d0/3d0)
       fact22=rate(mp2,mm2,s2,1d0+1d0/3d0)
       fact23=rate(mp2,mm2,s2,1d0+2d0/3d0)
       fact24=rate(mp2,mm2,s2,1d0+3d0/3d0)
       fact25=rate(mp2,mm2,s2,1d0+4d0/3d0)
       fact26=rate(mp2,mm2,s2,1d0+5d0/3d0)

       calcfrac=fs1**(5d0/3d0)*fs2**(0d0/3d0)*fact16*fact21&
          &+2d0*fs1**(4d0/3d0)*fs2**(1d0/3d0)*fact15*fact22&
              &+fs1**(3d0/3d0)*fs2**(2d0/3d0)*fact14*fact23&
              &+fs1**(2d0/3d0)*fs2**(3d0/3d0)*fact13*fact24&
          &+2d0*fs1**(1d0/3d0)*fs2**(4d0/3d0)*fact12*fact25&
              &+fs1**(0d0/3d0)*fs2**(5d0/3d0)*fact11*fact26


        case default

          calcfrac=1d0

  end select

end function


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1


double precision function rate(m1,m2,s,add)
  implicit none
  double precision, intent(in) :: m1,m2,s,add
  double precision :: s2
  double precision :: prec=1d-10

  s2=s+add

  if(abs(s2)<prec) then
    rate=dlog(m1/m2)
  else
    if(abs(s)>5) then
      rate=((m1/m2)**s2-1d0)/s2
    else
      rate=m2**s2*((m1/m2)**s2-1d0)/s2
    end if
  end if

 
end function


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

double precision function calckern(a1,a2,m1,m2)
  USE variables
  implicit none
  double precision, intent(in) :: a1,a2,m1,m2

  Select case(kernel)
    case ("C")
      calckern=2d0

    case ("A")
      calckern=m1+m2

    case ("O")
      calckern=pi*(a1+a2)**2d0*a1**0.5d0*&
           &dsqrt(8d0/3d0/pi)*dsqrt(3d0/dsqrt(8d0)*kap*dsqrt(kb*gg)*gam*rho_gr/(mu*mp)/dsqrt(nh*T))

    case default
       print*,""
       print*,"Unknown kernel"
       print*,"Choose 'C','A' or 'O'"
       stop
  end select

end function
