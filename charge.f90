subroutine charge(ipart)
  USE variables
  implicit none
  integer :: i,irho,iter
  integer, intent(in) :: ipart
  double precision :: prec=1d-6
  double precision :: ff,dfdpsi
  double precision :: dpsi,fpsi
  

  ! Calculate psi0
  B=se*dsqrt(mui*mp/me) 
  if(psi==0d0) then
    call calc_psi0()
    psi = psi0
  end if

  ! Initializing the variables
  sigv=2d-7*(T/300d0)**(alp)
  vi=dsqrt(8d0*kb*T/(pi*mui*mp))
  taubin(:)=abin(:,ipart)*kb*T/e**2d0
  alpha(:)=dsqrt(8d0/(pi*taubin(:)))
  fpsi=ff(ipart)

  ! Newton-Raphson loop
  iter=0
  do while (abs(fpsi) > prec .and. iter<100)
    dpsi=dfdpsi(ipart) !df/dpsi

    ! Update the psi
    psi = psi - fpsi/dpsi

    ! Limit psi between [psi_0:0]
    if(psi<psi0) then
      psi = psi0*0.9999999
    else if (psi>0) then
      psi=-1d-5
    end if

    fpsi=ff(ipart)
    iter=iter+1

    if(iter==100) then
      print*, "Convergence issue for psi"
      stop
    end if
  end do ! End NR 

  ! Calculate the charges
  ne=eps*ni

  print*, iter, "iterations of NR, psi=",psi,"f(psi)=",fpsi


end subroutine




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



double precision function ff(ipart)
! Function to find f(psi)=0
  USE variables
  implicit none
  integer, intent(in) :: ipart

  eps = (1d0-psi)/B*dexp(-psi)
  zbin(:,ipart)=psi*taubin(:) + (1d0-eps**2d0*B**2d0)/(1d0+eps*B*alpha(:)+eps**2d0*B**2d0)
  ni= -1d0/(1d0-eps)*sum(nbin(:,ipart)*zbin(:,ipart))
  jmoy(:)=(1d0-psi) + (2d0/taubin(:)*(eps**2d0*B**2d0+eps*B)/(1d0+eps*B*alpha(:)+eps**2d0*B**2d0))
  ff=sigv*eps*ni**2d0 + ni*vi*sum(nbin(:,ipart)*pi*abin(:,ipart)**2d0*jmoy(:))
  ff=ff/(xi*nh)-1d0

end function

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


double precision function dfdpsi(ipart)
  ! df/dpsi
  USE variables
  implicit none
  double precision :: dnidpsi,depsdpsi
  integer, intent(in) :: ipart

  depsdpsi=-eps*(2d0-psi)/(1d0-psi)
  dnidpsi=-1d0/(1d0-eps)**2d0 * depsdpsi * sum(nbin(:,ipart)/(1d0+eps*B*alpha(:)+eps**2d0*B**2d0)**2d0 &
       &*(-eps**4d0*B**4d0 + (4d0*B**2d0-B**3d0*alpha(:))*eps**2d0+(2d0*B*alpha(:)-4d0*B**2d0)*eps+1d0-B*alpha(:)))&
       &-1d0/(1d0-eps)*(psi/(1d0-eps)*depsdpsi + 1d0) * sum(nbin(:,ipart)*taubin(:))
  dfdpsi= sigv*ni*(ni*depsdpsi+2d0*eps*dnidpsi) + 2d0*ni*vi*(eps**2d0*B**2d0+eps*B)*&
        &((1d0/ni*dnidpsi + (2d0*eps*B+1d0)/(eps*(1d0+eps*B))*depsdpsi)*&
        & sum(nbin(:,ipart)*pi*abin(:,ipart)**2d0/(taubin(:)*(1d0+eps*B*alpha(:)+eps**2d0*B**2d0)))&
        & +depsdpsi*sum(nbin(:,ipart)*pi*abin(:,ipart)**2d0*(2d0*eps*B**2d0+B*alpha(:))&
                      &/(taubin(:)*(1d0+eps*B*alpha(:)+eps**2d0*B**2d0)**2d0)))&
        &+ni*vi*(1d0/ni*dnidpsi * (1d0-psi)-1d0 ) * sum(nbin(:,ipart)*pi*abin(:,ipart)**2d0)

  dfdpsi=dfdpsi/(xi*nh)
  
end function


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



Subroutine calc_psi0()
  ! Calculate psi0 with a genetic algorithm
  USE variables 
  implicit none
  integer :: i,im1,im2,iter
  integer,parameter :: n=10
  integer,parameter :: seed=124353
  double precision :: psimin, psimax, psitop,psibot
  double precision :: prec = 1d-10
  double precision :: eps0 = 0.9999
  double precision :: ave,maxf1,maxf2
  double precision :: fpsi0
  double precision,dimension(n) :: psirand,frand

  psimin = -5
  psimax = -1

  eps0=0.99999

  ! Initialize
  call srand(seed)
  do i=1,n
    psirand(i)=rand()*(psimax-psimin)+psimin
  end do

  psitop=maxval(psirand(:))
  psibot=minval(psirand(:))

  iter=0
  ! Loop genetic algo
  do while (abs(psitop-psibot) > prec)
    iter=iter+1

    ! Calculate function for psis
    do i=1,n
      frand(i)=fpsi0(psirand(i),eps0,B)
    end do

    ! Find the two worst values
    maxf1=abs(frand(1))
    maxf2=abs(frand(1))
    im1=1
    im2=1
    do i=2,n
      if(abs(frand(i))>maxf1) then
        maxf2=maxf1
        maxf1=abs(frand(i))
        im2=im1
        im1=i
       else if (abs(frand(i))>maxf2) then
         maxf2=abs(frand(i))
         im2=i
       end if
     end do

     ! Replace one by the average
     psirand(im1)=0
     do i=1,n
       if(i .ne. im1 .and. i .ne. im2) psirand(im1)=psirand(im1)+psirand(i)
     end do
     psirand(im1)=psirand(im1)/dble(n-2)
     psirand(im2)=psirand(im1)

     psitop=maxval(psirand(:))
     psibot=minval(psirand(:))

     ! Replace the other by another random
     psirand(im2)=rand()*(psimax-psimin)+psimin


   end do

   psi0=0.5d0*(psitop+psibot)

end subroutine

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

double precision function fpsi0(pp,ee,bb)
  ! Function to calculate psi0
  implicit none
  double precision, intent(in) ::pp,ee,bb
  fpsi0=(1d0-pp)/(ee*bb)*dexp(-pp) -1d0
end function




  



