MODULE variables
  ! Variables declaration
  implicit none


  ! Constants
  double precision,parameter :: mp=1.67e-24   ! Proton mass
  double precision,parameter :: me=9.109d-28  ! Electron mass
  double precision,parameter :: kb=1.38e-16   ! Botlzmann constant
  double precision,parameter :: pi=3.1415926  ! pi
  double precision,parameter :: gg=6.67e-8    ! Gravitational constant
  double precision,parameter :: e=4.8d-10     ! Electric charge
  double precision,parameter :: c=3d10        ! Light speed
  double precision,parameter :: gam=1.66667   ! Adiabatic index
  
  ! Gas
  double precision :: nh=1d4                  ! Density
  double precision :: T=1d1                   ! Temperature
  double precision :: mu=2.3                  ! Mean atomic weight
  double precision,parameter :: alp=-5d-1     ! For collision rate

  ! Grain distribution
  integer :: nbinmax=50                       ! Number of size bins 
  integer :: nbinini=50                       ! Number of size bins of the initial distribution
  double precision :: amin=1.5e-6             ! Minimum size of grains in cm
  double precision :: amindis=1.0e-6          ! Minimum size of the initial distribution in cm
  double precision :: amaxdis=2.5e-5          ! Maximum size of the initial distribution in cm
  double precision :: amax=1.0e-3             ! Maximum size of grains in cm
  double precision :: rho_gr=2.3    ! Grain bulk density
  double precision :: slope_ini=-3.5          ! Slope of the initial distribution if kern=M
  double precision :: dusttogas=1d-2          ! dust to gas ratio
  double precision :: massmin,massmax,massmaxdis,massmindis ! Min and max mass of the distribution
  double precision,dimension(:),allocatable :: rbinmin,rbinmax,mbinmin,mbinmax      ! Bounds of the bins in size and mass
  double precision,dimension(:),allocatable :: taubin,alpha,jmoy                    ! Reduced temperature and charge of the grains
  double precision,dimension(:,:),allocatable :: massbin,rhobin,slopem,slope,nbin   ! Properties of the bins (average mass,density,slope of the distribution,number density
  double precision,dimension(:,:),allocatable :: abin,a2bin,a3bin,zbin              ! Moments of the distribution and electric charge
  double precision,dimension(:,:),allocatable :: drhobin                            ! Variation of density for each bin
  double precision :: kap=2.97                ! Value of function f/z (approximation for the intermediate coupling, cf Guillet+2020
  double precision :: deltm,deltp             ! To calculate the slopes
  double precision :: eta,zeta                ! Mass and size ratio between bins
  character(3) :: distrib="mrn"               ! Initial size distribution. "mrn"=mrn-like power-law, "con"=test with constant kernel, "add"=test with additive kernel

  ! Integration & print
  integer :: ntimesout                        ! Number of output times 
  double precision,dimension(:),allocatable ::times  ! Output times
  double precision :: timemax=1e9             ! Time max
  double precision :: timeoutstart=2e5        ! Time of the first dump
  double precision :: chistart=1e16           ! Chi of the first dump
  double precision :: chimax=1e18             ! Chi max
  double precision :: varfact=0.8             ! Max relative variation allowed in one bin
  character(3) :: timespacetype="log"         ! "log"=logarithmic spacing, "lin"=linear spacing
  double precision :: time                    ! Current time
  double precision :: chi                     ! Reduced variable chi
  integer :: ntimes=1                         ! Number of time outputs

  ! For particles
  integer :: ntimespart=200                   ! Number of timesteps for "traj"
  integer :: npart=1                          ! Total number of particles

  ! Coagulation
  character(1) :: kernel="O"   ! "O"=Ormel, "C"=Constant, "A"=Additive

  ! Ionization parameters
  double precision :: mui=25.0       ! Ion mean mass
  double precision :: se=0.5         ! Sticking probability
  double precision :: xi=5d-17       ! Ionisation rate
  double precision :: B              ! B for function with epsilon

  ! Ionization variables
  double precision :: sigv   ! Collision rate
  double precision :: vi     ! thermal velocity of grains
  double precision :: psi    ! Spitzer variable
  double precision :: eps    ! ne/ni
  double precision :: ni,ne  ! Ion and electron density
  double precision :: psi0   ! Initial psi

  ! Resistivities
  double precision :: Bmag = 3d-5              ! Magnetic field in G
  character(4) :: mag_type="pres"              ! Type of prescription : "cons"= constant field, "pres"=prescription (see routine "resistivities", Li et al 2009 by default)
  logical :: calc_resist=.false.               ! Enable resistivity calculation
  double precision :: eta_ohm,eta_hal,eta_amb  ! Resistivities

  ! Others
  character(127):: outfile,infile             ! Input and output files
  character(4) :: calctype="coag"             ! "traj"=particle trajectories, "coag"=Constant density/temperature, "chit"=Function of chi
  double precision,dimension(401) :: printout   ! To format the output
  logical :: calc_charge=.true.               ! Calculate charge or not
  character(3) :: output_format="std"            ! Format of the output file, "std" or "tbl". "std"= mass/radius/rho/n/X/slope/nH/Z/ni/ne -> good for analysis. "tbl"=(radius/cross-section/mass/X)*nbinmax -> Necessary info for hydro simulations

end module
