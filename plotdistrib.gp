set term post enh color
set output "distrib.ps"

set xlabel "a ({/Symbol m}m)" font "Times-Roman,20"
set ylabel "{/Symbol r}_{bin}" font "Times-Roman,20"

set xtics font "Times-Roman,20"
set ytics font "Times-Roman,20"

set format "%g"
set format y "10^{%L}"

set logscale

set xrange[1e-3:2e1]
set yrange [1e-5:1e-2]


plot "distrib_chi.dat" index 1 u ($2*1e4):5 w lp lc 1 lw 2 tit "t=0",\
     "distrib_chi.dat" index 2 u ($2*1e4):5 w lp lc 2 lw 2 tit "t=tmax"
