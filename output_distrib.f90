subroutine output_distrib(ini,ipart)
  USE variables
  USE part_trajectories
  implicit none
  integer,intent(in) :: ipart
  integer ::i,j,nwr
  logical,intent(in) :: ini
  character(3) :: ichara,suff
  character(27) :: addhead
  character(3) :: icol,ibin
  character(10000) :: header

  ! Open file
  if(npart==1) then
    open(23,file=trim(adjustl(outfile)),access="append")
    if(calc_resist) open(24,file="resists.dat",access="append")
  else
    write(ichara,'(I3)') ipart
    if(ipart<10) then
      suff="00"//trim(adjustl(ichara))
    else if(ipart<100) then
      suff="0"//trim(adjustl(ichara))
    else if(ipart<1000) then
      suff=trim(adjustl(ichara))
    else
      print*, "Too many particles (>999)!" 
      stop
    end if
    open(23,file=trim(adjustl(outfile))//"_"//suff,access="append")
    if(calc_resist) open(24,file="resists.dat_"//suff,access="append")
  end if


  ! Header
  if(ini) then
    if(output_format=="std" .or. distrib .ne. "mrn") then
      header=                       "  1     Mass_bin (g)      |"
      header=trim(adjustl(header))//"  2   Radius_bin (cm)     |"
      header=trim(adjustl(header))//"  3   rho_bin (g cm-3)    |"
      header=trim(adjustl(header))//"  4     n_bin (cm-3)      |"
      header=trim(adjustl(header))//"  5  X_bin (=rho/nh/mp)   |"
      header=trim(adjustl(header))//"  6  slope dn/da          |"
      header=trim(adjustl(header))//"  7  Chi (cgs)            |"
      if(calc_charge) then
      header=trim(adjustl(header))//"  8    <z_bin>            |"
      header=trim(adjustl(header))//"  9     ni (cm-3)         |"
      header=trim(adjustl(header))//"  10    ne (cm-3)         |"
      end if
    elseif(output_format=="tbl") then
      header=                       "  1     chi (cgs)         |"
      do i=1,nbinmax
        write(icol,"(i3)") 1+(i-1)*4+1
        write(ibin,"(i3)") i
        addhead=" "//icol//"    abin "//ibin//" (cm)     |"
        header=trim(adjustl(header))//addhead

        write(icol,"(i3)") 1+(i-1)*4+2
        addhead=" "//icol//"    a2bin "//ibin//" (cm2)   |"
        header=trim(adjustl(header))//addhead

        write(icol,"(i3)") 1+(i-1)*4+3
        addhead=" "//icol//"    mbin "//ibin//"  (g)    |"
        header=trim(adjustl(header))//addhead

        write(icol,"(i3)") 1+(i-1)*4+4
        addhead="  "//icol//" Xbin "//ibin//" (nbin/nh) |"
        header=trim(adjustl(header))//addhead
      end do
    end if
    write(23,*) trim(adjustl(header))
    write(23,"(i4,A,i3)") ntimesout+1, ",",nbinmax
    write(23,*) 
    write(23,*) 
  end if

   nwr=7
   if(calc_charge) nwr=10
   if(distrib=="con" .or. distrib=="add") nwr=6




  ! Write distribution
  if(output_format=="std" .or. distrib .ne. "mrn") then

    j=max(ipart,1)
    do i=1,nbinmax
        printout(1)=massbin(i,j)
        printout(2)=abin(i,j)
        printout(3)=rhobin(i,j)
        printout(4)=nbin(i,j)
        printout(5)=rhobin(i,j)/(mu*mp*nh)
        printout(6)=slope(i,j)
        printout(7)=chi
        if(calc_charge) then
          printout(8)=zbin(i,j)
          printout(9)=ni
          printout(10)=ne
        end if


      if(ini) then
        write(23,*) printout(1:nwr)
      else

        Select case (distrib)
          case("mrn")
            write(23,*) printout(1:nwr)

          case("con")
            if(mbinmin(i)/time<1d0) then
              write(23,*) printout(1:nwr),max((1d0+mbinmin(i)/time)*dexp(-mbinmin(i)/time)-&
                                         &(1d0+mbinmax(i)/time)*dexp(-mbinmax(i)/time),0d0)
            else
              write(23,*) printout(1:nwr),max(dexp(-mbinmin(i)/time)*((1d0+mbinmin(i)/time)-&
                                          &(1d0+mbinmax(i)/time)*dexp((-mbinmax(i)+mbinmin(i))/time)),0d0)
            end if

          case ("add")
            if(mbinmin(i)*dexp(-2d0*time)<1d0) then
              write(23,*) printout(1:nwr),(derf(dsqrt(mbinmax(i)*dexp(-2d0*time)/2d0))-&
                                      &derf(dsqrt(mbinmin(i)*dexp(-2d0*time)/2d0)))
            else
              write(23,*) printout(1:nwr),(-derfc(dsqrt(mbinmax(i)*dexp(-2d0*time)/2d0))+&
                                       &derfc(dsqrt(mbinmin(i)*dexp(-2d0*time)/2d0)))
            end if
        end select

      end if

    end do

    write(23,*)
    write(23,*)

    close(23)

    if(calc_resist) then
      write(24,*) time,nh,T,eta_ohm,eta_hal,eta_amb
      close(24)
    end if



  elseif(output_format=="tbl") then
    ! Write in table format

    j=max(ipart,1)
    do i=1,nbinmax
      printout(1)=chi
      printout(4*(i-1)+1+1)=abin(i,j)
      printout(4*(i-1)+2+1)=a2bin(i,j)*pi
      printout(4*(i-1)+3+1)=massbin(i,j)
      printout(4*(i-1)+4+1)=nbin(i,j)/(nh)
    end do
    write(23,*) printout(1:4*nbinmax+1)

  end if





end subroutine
