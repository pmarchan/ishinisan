subroutine resistivities(ipart)
  USE variables
  implicit none
  integer :: i
  integer,intent(in)::ipart
  double precision :: tau_en,tau_in,sig_e,sig_i,w_e,w_i,scoll_e,scoll_i ! Collision and cyclotron frequencies for electrons and ions
  double precision,dimension(:),allocatable :: tau_gn,sig_g,w_g,scoll_g ! Collision and cyclotron frequencies for grains
  double precision :: sig_par,sig_ort,sig_hal                           ! Conductivities

  allocate(tau_gn(nbinmax),sig_g(nbinmax),w_g(nbinmax),scoll_g(nbinmax))

  if (mag_type == "pres") then
    Bmag=1.43d-7*dsqrt(nh) ! Li et al 2009 prescription for protostellar collapse
  end if

  ! Marchand et al 2016

  ! Collision rates from Pinto & Galli 2008 (velocities in km/s)
  scoll_e = 3.16d-11 * (dsqrt(8d0*kb*T/(pi*me))*1d-5)**1.3d0
  scoll_i = 2.40d-9 * (dsqrt(8d0*kb*T/(pi*(mui*mp*2d0*mp)/(mui*mp+2d0*mp)))*1d-5)**0.6d0 ! Assuming the rate for HCO+ ions
  scoll_g(:) = pi*abin(:,ipart)**2d0 * dsqrt(8d0*kb*T/(pi*2d0*mp))&
               &*(1d0+dsqrt(pi*e**2d0/(2d0*abin(:,ipart)*kb*T)))
  !scoll_g(:) = pi*abin(:,ipart)**2d0 * dsqrt(8d0*kb*T/(pi*massbin(:,ipart)))&
               !&*(1d0+dsqrt(pi*e**2d0/(2d0*abin(:,ipart)*kb*T)))

  ! Collision times with H2
  tau_en = 1d0/1.16d0 * (me+2d0*mp)/(2d0*mp) * 1d0/(nh/2d0*scoll_e)
  tau_in = 1d0/1.14d0 * (mui*mp+2d0*mp)/(2d0*mp) * 1d0/(nh/2d0*scoll_i)
  tau_gn(:) = 1d0/1.28d0 * (massbin(:,ipart)+2d0*mp)/(2d0*mp) * 1d0/(nh/2d0*scoll_g(:))

  ! Conductivity
  sig_e = ne*e**2d0*tau_en/me
  sig_i = ni*e**2d0*tau_in/(mui*mp)
  sig_g(:) = nbin(:,ipart)*(zbin(:,ipart)*e)**2d0*tau_gn(:)/massbin(:,ipart)

  ! Cyclotron frequency
  w_e = -e*Bmag/(me*c)
  w_i = e*Bmag/(mui*mp*c)
  w_g(:) = zbin(:,ipart)*e*Bmag/(massbin(:,ipart)*c)

  ! Conductivities
  ! Parallel
  sig_par = sig_e+sig_i+sum(sig_g(:))
  ! Orthogonal
  sig_ort = sig_e/(1d0+(w_e*tau_en)**2d0) + sig_i/(1d0+(w_i*tau_in)**2d0)&
         &+ sum(sig_g(:)/(1d0+(w_g(:)*tau_gn(:))**2d0))
  ! Hall
  sig_hal = -sig_e*(w_e*tau_en)/(1d0+(w_e*tau_en)**2d0) - sig_i*(w_i*tau_in)/(1d0+(w_i*tau_in)**2d0) &
          & -sum(sig_g(:)*(w_g(:)*tau_gn(:))/(1d0+(w_g(:)*tau_gn(:))**2d0))

  ! Resistivities
  eta_ohm = 1d0/sig_par
  eta_hal = sig_hal/(sig_ort**2d0+sig_hal**2d0)
  eta_amb = sig_ort/(sig_ort**2d0+sig_hal**2d0) - 1d0/sig_par


end subroutine
