set term post enh color
set output "distrib_coagulation.ps"

set xlabel "Grain size ({/Symbol m}m)" font "Times-Roman,20"
set ylabel "d{/Symbol r}/dlog(a)/({/Symbol m}m_Hn_H)" font "Times-Roman,20"

set xtics font "Times-Roman,20"
set ytics font "Times-Roman,20"

set format y "10^{%L}"
set format x "%g"

set logscale

#set xrange[1e-5:1e8]
#set yrange [1e-10:1e0]
set key bot right

plot "distrib_coagulation.dat" index 0 u ($2/1e-4):5 w lp lc 1 lw 2 tit "t=0",\
     "distrib_coagulation.dat" index 1 u ($2/1e-4):5 w lp lc 2 lw 2 tit "t=1 Myr",\
     "distrib_coagulation.dat" index 2 u ($2/1e-4):5 w lp lc 3 lw 2 tit "t=3.1 Myr",\
     "distrib_coagulation.dat" index 3 u ($2/1e-4):5 w lp lc 4 lw 2 tit "t=10 Myr",\
     "distrib_coagulation.dat" index 4 u ($2/1e-4):5 w lp lc 5 lw 2 tit "t=31 Myr",\
     "distrib_coagulation.dat" index 5 u ($2/1e-4):5 w lp lc 6 lw 2 tit "t=100 Myr"
