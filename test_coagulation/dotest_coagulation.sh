#!/bin/bash

if [ $1 == "clean" ]
then
  rm distrib_coagulation.dat
  rm distrib_coagulation.ps
else
  cd ..
  make
  ./ishinisan test_coagulation/test_coagulation.nml
  mv distrib_coagulation.dat test_coagulation/
  cd test_coagulation
  gnuplot plot_coagulation.gp
  gv distrib_coagulation.ps
fi
