subroutine read_params(paramfile)
  ! Read the parameter file
  USE variables
  USE part_trajectories
  implicit none
  character(127),intent(in) :: paramfile

  ! Namelist def
  namelist/inpout_params/outfile,ntimesout,timespacetype,timeoutstart,timemax,chistart,chimax,calctype,output_format
  namelist/trajectories/ntimespart,npart,infile
  namelist/gas_params/nh,T,mu
  namelist/grain_distrib/nbinmax,amin,amindis,amaxdis,amax,rho_gr,slope_ini,dusttogas,distrib
  namelist/coagulation_algo/kernel,varfact
  namelist/ionization/calc_charge,mui,se,xi,calc_resist,mag_type,Bmag

  ! Read namelist
  open(55,file=trim(adjustl(paramfile)),status="old")
  read(55,NML=inpout_params)
  rewind(55)
  read(55,NML=trajectories)
  rewind(55)
  read(55,NML=gas_params)
  rewind(55)
  read(55,NML=grain_distrib)
  rewind(55)
  read(55,NML=coagulation_algo)
  rewind(55)
  read(55,NML=ionization)
  close(55)

end subroutine

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

subroutine alloc()
  ! Allocate the arrays
  USE variables
  USE part_trajectories
  implicit none

  if(calctype=="traj") then
    allocate(partx(npart,ntimespart,3),partv(npart,ntimespart,3))
    allocate(partrho(npart,ntimespart),parttemp(npart,ntimespart))
    allocate(parttime(ntimespart))
  end if

  allocate(nhold(npart))
  allocate(rbinmin(nbinmax),rbinmax(nbinmax),mbinmin(nbinmax),mbinmax(nbinmax))
  allocate(massbin(nbinmax,npart),rhobin(nbinmax,npart),slopem(nbinmax,npart),slope(nbinmax,npart),nbin(nbinmax,npart))
  allocate(abin(nbinmax,npart),a2bin(nbinmax,npart),a3bin(nbinmax,npart))
  allocate(zbin(nbinmax,npart),taubin(nbinmax),alpha(nbinmax),jmoy(nbinmax))
  allocate(drhobin(nbinmax,npart))
  allocate(times(ntimesout))

end subroutine
