program main
  implicit none
  integer :: ntot,nbin
  integer :: i,j
  double precision, dimension(:,:),allocatable :: readta


  open(10,file="table_ng.dat",status="old")

  read(10,*)
  read(10,*)ntot,nbin
  allocate(readta(ntot,4*nbin+1))
  read(10,*)
  read(10,*)
  do i=1,ntot
  read(10,*) readta(i,:)
  end do
  close(10)


  open(11,file="table_coag.dat",status="replace",form="unformatted")
  write(11) nbin,ntot
  write(11) readta
  close(11)

end program

