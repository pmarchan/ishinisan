MODULE part_trajectories
  USE variables
  implicit none
  ! Write here if you want user-defined trajectory data. You need at least partrho (density), parttemp (temperature) and parttime (time)
  double precision,dimension(:,:,:),allocatable :: partx,partv
  double precision,dimension(:,:),allocatable :: partrho,parttemp,partmag
  double precision,dimension(:),allocatable :: parttime
  double precision,dimension(:),allocatable :: nhold



CONTAINS

  subroutine initialize_trajectories()
    ! This routine read the trajectory data file
    USE variables
    implicit none
    double precision :: dum
    integer :: i,j
  
    open(10,file=trim(adjustl(infile)),status="old")
    print*, "Reading trajectories..."
  ! Write here if you want user-defined trajectory data. You need at least partrho (density), parttemp (temperature) and parttime (time)
    do i=1,npart
      do j=1,ntimespart
        !read(10,*) partx(i,j,1:3),partv(i,j,1:3),partrho(i,j),parttemp(i,j),partmag(:,:),dum,parttime(j)
        read(10,*) partx(i,j,1:3),partv(i,j,1:3),partrho(i,j),parttemp(i,j),dum,parttime(j)
      end do
      read(10,*)
      read(10,*)
    end do
    close(10)
    parttime(:)=parttime(:)*1d3*86400*365.25  ! Convert time from kyr to s
    partrho(:,:)=partrho(:,:)/(3.83e-24)      ! Convert density from g cm-3 to cm-3
    timemax=parttime(ntimespart-1)
    print*, "Trajectories OK"
    nh=partrho(1,1)
  
  end subroutine

end module
