subroutine calcslopes(ipart)
  ! Subroutine to calculate the slopes and moments of the distribution
  USE variables
  USE part_trajectories
  implicit none
  integer :: i
  integer, intent(in):: ipart

  ! Calculate the slopes. slopem= dn/dm. slope=dn/da
  do i=1,nbinmax
      if(i==1) then
        deltp=dlog(rhobin(i+1,ipart)/rhobin(i,ipart)/eta)
        slopem(i,ipart)=deltp/dlog(eta)-1d0
      else if (i==nbinmax) then
        deltm=dlog(rhobin(i,ipart)/rhobin(i-1,ipart)/eta)
        slopem(i,ipart)=deltm/dlog(eta)-1d0
      else
        deltm=dlog(rhobin(i,ipart)/rhobin(i-1,ipart)/eta)
        deltp=dlog(rhobin(i+1,ipart)/rhobin(i,ipart)/eta)
        if(deltm*deltp<0) then
          slopem(i,ipart)=deltp/dlog(eta)-1d0
        else
          slopem(i,ipart)=2d0*deltm*deltp/(deltm+deltp)/dlog(eta)-1d0
        end if
      end if
      slope(i,ipart)=3d0*slopem(i,ipart)+2d0
  end do
  ! Calculate the moments of the distribution
  do i=1,nbinmax
    if(abs(slope(i,ipart)+2d0)/2d0<1d-14) then
      abin(i,ipart)=rbinmin(i)*dlog(zeta)/(1d0-zeta**(-1d0))
    else
      abin(i,ipart) =rbinmin(i)**1d0*(slope(i,ipart)+1d0)/(slope(i,ipart)+2d0)*(zeta**(slope(i,ipart)+2d0)-1d0)&
                     &/(zeta**(slope(i,ipart)+1d0)-1d0)
    end if
    if(abs(slope(i,ipart)+3d0)/3d0<1d-14) then
      a2bin(i,ipart)=2d0*rbinmin(i)**2d0*dlog(zeta)/(1d0-zeta**(-2d0))
    else
      a2bin(i,ipart)=rbinmin(i)**2d0*(slope(i,ipart)+1d0)/(slope(i,ipart)+3d0)*(zeta**(slope(i,ipart)+3d0)-1d0)&
                     &/(zeta**(slope(i,ipart)+1d0)-1d0)
    end if
    if(abs(slope(i,ipart)+4d0)/4d0<1d-14) then
      a3bin(i,ipart)=3d0*rbinmin(i)**3d0*dlog(zeta)/(1d0-zeta**(-3d0))
    else
      a3bin(i,ipart)=rbinmin(i)**3d0*(slope(i,ipart)+1d0)/(slope(i,ipart)+4d0)*(zeta**(slope(i,ipart)+4d0)-1d0)&
                     &/(zeta**(slope(i,ipart)+1d0)-1d0)
    end if
    massbin(i,ipart)=4d0/3d0*pi*rho_gr*a3bin(i,ipart)
    nbin(i,ipart)=rhobin(i,ipart)/massbin(i,ipart)
  end do


end subroutine
