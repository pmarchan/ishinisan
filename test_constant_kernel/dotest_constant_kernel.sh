#!/bin/bash

if [ $1 == "clean" ]
then
  rm distrib_constant_kernel.dat
  rm distrib_constant_kernel.ps
else
  cd ..
  make
  ./ishinisan test_constant_kernel/test_constant_kernel.nml
  mv distrib_constant_kernel.dat test_constant_kernel/
  cd test_constant_kernel
  gnuplot plot_constant_kernel.gp
  gv distrib_constant_kernel.ps
fi
