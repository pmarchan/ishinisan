subroutine initialize
  USE variables
  USE part_trajectories
  implicit none
  integer :: i,ipart
  integer :: nbinini_u,nbinini_l ! Min and max bin of the initial distribution
  character(3) :: ichara,suff

  ! For the test cases
  select case (distrib)
    case("con")
      ntimesout=6
      kernel="C"
    case("add")
      ntimesout=4
      kernel="A"
    case("mrn")
      ntimesout=ntimesout
    case default
      print*, ""
      print*, "Unknown distribution"
      print*, "Choose 'mrn','con' or 'add'"
      stop
  end select


  ! Allocate arrays
  if(calctype .ne. "traj")  npart=1
  call alloc()


  ! Max times
  Select case (calctype)
    case ("traj")
      call initialize_trajectories()
    case ("chit")
      timeoutstart=chistart/(nh**0.75*T**(-0.25))
      timemax=chimax/(nh**0.75*T**(-0.25))
    case ("coag")
      timemax=timemax*86400d0*365.25d0
      timeoutstart=timeoutstart*86400d0*365.25d0
    case default
      print*, ""
      print*, "Unknown calculation type"
      print*, "Choose 'traj','coag' or 'chit'"
      stop
  end select
  print*, "Coagulation until t_max =", timemax/86400/365.25, "yr"


  ! Setup the output times
  if(calctype=="chit" .or. calctype=="coag") then

    select case (distrib)
      case("con")
        timemax=1d6
        ntimesout=6
        do i=1,ntimesout
          times(i)=10**dble(i)
        end do

      case("add")
        timemax=8
        ntimesout=4
        do i=1,ntimesout
          times(i)=2d0*dble(i)
        end do

      case("mrn")

        select case (timespacetype)

          case("lin")
            if(ntimesout==1) then
              times(1)=timemax
            else
              do i=1,ntimesout
                 times(i)=timeoutstart+(timemax-timeoutstart)*dble(i-1)/dble(ntimesout-1)
              end do
            end if
            write(*,*) "Linear time spacing choosen. Will output at"

          case("log")
            if(ntimesout==1) then
              times(1)=timemax
            else
              do i=1,ntimesout
                 times(i)=timeoutstart*(timemax/timeoutstart)**(dble(i-1)/dble(ntimesout-1))
              end do
            end if
            write(*,'(A43,X,e8.3,2X,A2)') "Log time spacing choosen. Will output at"

          case default
            print*, ""
            print*, "Unknown spacing"
            print*, "Choose 'lin' or 'log"
            stop
         end select

         !do i=1,ntimesout
           !if(calctype=="chit") then
             !write(*,'(A3,X,e8.3)') "Chi=",times(i)*nh**0.75*T**(-0.25)
           !else
             !write(*,'(A2,X,e8.3,2X,A2)') "t=",times(i)/(86400d0*365.25), "yr"
           !end if
         !end do
         print*,
    end select

  end if

  if(calctype=="traj") ntimesout=ntimespart

  ! Calculate the bin size intervals
  if(amin > amindis) then
    print*, "!!!!!!!"
    print*, "WARNING"
    print*, "amin > amindis"
    print*, "Shifting amin"
    print*, ""
    amin=amindis
  end if
  if(amax < amaxdis) then
    print*, "!!!!!!!"
    print*, "WARNING"
    print*, "amax < amaxdis"
    print*, "Shifting amax"
    print*, ""
    amax=amaxdis
  end if
  massmin=(4d0/3d0*pi*rho_gr*amin**3d0)
  massmindis=(4d0/3d0*pi*rho_gr*amindis**3d0)
  massmaxdis=(4d0/3d0*pi*rho_gr*amaxdis**3d0)
  massmax=(4d0/3d0*pi*rho_gr*amax**3d0)
  eta=(massmax/massmin)**(1d0/dble(nbinmax))
  zeta=eta**(1d0/3d0)


  ! m-, m+, a-, a+ for all bins
  nbinini_l=1
  nbinini_u=nbinmax
  do i=1,nbinmax
    mbinmin(i)=massmin*eta**dble(i-1)
    mbinmax(i)=massmin*eta**dble(i)
    rbinmin(i)=amin*zeta**dble(i-1)
    rbinmax(i)=amin*zeta**dble(i)
    if(mbinmin(i)<massmindis) nbinini_l=i
    if(mbinmin(i)<massmaxdis) nbinini_u=i
  end do
  nbinini_l=nbinini_l+1
  if(kernel=="C".or.kernel=="A") then
    nbinini=nbinmax
  else
    nbinini=nbinini_u-nbinini_l+1
    print*, "amin =", amindis/1e-7, "nm"
    print*, "amax =", amaxdis/1e-7, "nm"
    print*, "Bins of the initial distribution=",nbinini
  end if


  ! Initial conditions
  if(calctype=="traj")  nh=partrho(1,1)

  Select case (distrib)

    case("mrn")
      ! MRN, or any slope : dn/da \propto a^{\lambda}
      do i=nbinini_l,nbinini_u
        rhobin(i,:)=mu*mp*nh*dusttogas*(rbinmax(i)**(slope_ini+4d0)-rbinmin(i)**(slope_ini+4d0))/&
                                         &(amaxdis**(slope_ini+4d0)-amindis**(slope_ini+4d0))
      end do
      do i=1,nbinini_l-1
        rhobin(i,:)=rhobin(nbinini_u,:)*1e-20
      end do
      do i=nbinini_u+1,nbinmax
        rhobin(i,:)=rhobin(nbinini_u,:)*1e-20
      end do

    case("con")
      ! Constant kernel, cf Guillet+2020
      do i=1,nbinmax
        if(mbinmin(i)<1d0) then
          rhobin(i,1)=(1d0+mbinmin(i))*dexp(-mbinmin(i))-(1d0+mbinmax(i))*dexp(-mbinmax(i))
        else
          rhobin(i,1)=dexp(-mbinmin(i))*((1d0+mbinmin(i))-(1d0+mbinmax(i))*dexp(-mbinmax(i)+mbinmin(i)))
        end if
      end do
      timemax=10**6

    case("add")
      ! Additive kernel, cf Guillet+2020
      do i=1,nbinmax
        if(mbinmin(i)<1d0) then
          rhobin(i,1)=derf(dsqrt(mbinmax(i)/2d0))-derf(dsqrt(mbinmin(i)/2d0))
        else
          rhobin(i,1)=-derfc(dsqrt(mbinmax(i)/2d0))+derfc(dsqrt(mbinmin(i)/2d0))
        end if
      end do

  end select

  rhobin(:,:)=max(rhobin(:,:),1d-300)

  ! Create output file(s)
  if(calctype=="traj" .and. npart>1) then
    do ipart=1,npart
      write(ichara,'(I3)') ipart
      if(ipart<10) then
        suff="00"//trim(adjustl(ichara))
      else if(ipart<100) then
        suff="0"//trim(adjustl(ichara))
      else if(ipart<1000) then
        suff=trim(adjustl(ichara))
      else
        print*, "Too many particles (>999)!" 
        stop
      end if
      open(23,file=trim(adjustl(outfile))//"_"//suff,status="replace")
      if(calc_resist) open(24,file="resists.dat_"//suff,status="replace")
    end do
  else
    open(23,file=trim(adjustl(outfile)),status="replace")
    if(calc_resist) open(24,file="resists.dat",status="replace")
  end if

  close(23)
  close(24)


  ! Electric charge
  zbin(:,:)=0d0

end subroutine
