  FC = gfortran
  #FOPT = -g3 -O0 -Wall -Wextra -fbounds-check -fimplicit-none -std=f95 -pedantic -Waliasing
  FOPT = -g -O2 -fbounds-check -fbacktrace

  OBJ= variables.o \
       part_trajectories.o \
       calcslopes.o \
       initialize.o \
       output_distrib.o \
       read_alloc.o \
       charge.o \
       resistivities.o

all: ishinisan

variables.o: variables.f90
	$(FC) $(FOPT) -c variables.f90

part_trajectories.o: part_trajectories.f90
	$(FC) $(FOPT) -c part_trajectories.f90

read_alloc.o: read_alloc.f90
	$(FC) $(FOPT) -c read_alloc.f90

calcslopes.o: calcslopes.f90
	$(FC) $(FOPT) -c calcslopes.f90

initialize.o: initialize.f90
	$(FC) $(FOPT) -c initialize.f90

output_distrib.o: output_distrib.f90
	$(FC) $(FOPT) -c output_distrib.f90

charge.o: charge.f90
	$(FC) $(FOPT) -c charge.f90

resistivities.o: resistivities.f90
	$(FC) $(FOPT) -c resistivities.f90

ishinisan: $(OBJ) ishinisan.f90
	$(FC) $(FOPT) $(OBJ) ishinisan.f90 -o ishinisan

clean:
	rm -f *.o *.mod
